function ex1() {
  var day = document.getElementById("txt-day")?.value * 1;
  var month = document.getElementById("txt-month")?.value * 1;
  var year = document.getElementById("txt-year")?.value * 1;
  var dayPre = day - 1;
  var dayNext = day + 1;
  var monthPre = month;
  var monthNext = month;
  var yearPre = year;
  var yearNext = year;

  if (month >= 1 && month <= 12) {
    switch (month) {
      case 1:
        if (day <= 1) {
          dayPre = 31;
          monthPre = 12;
          yearPre = year - 1;
          break;
        }
      case 3:
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
          if (day <= 1) {
            dayPre = 29;
            monthPre -= 1;
          }
          break;
        } else {
          if (day <= 1) {
            dayPre = 28;
            monthPre -= 1;
            break;
          }
        }
      case 5:
      case 7:
      case 8:
        if (day <= 1 && month == 8) {
          dayPre = 31;
          monthPre = 7;
          break;
        }
      case 10:
      case 12:
        if (day >= 31) {
          dayNext = 1;
          if (month == 12) {
            monthNext = 1;
            yearNext = year + 1;
          } else {
            monthNext += 1;
          }
        } else if (day <= 1) {
          dayPre = 30;
          monthPre -= 1;
        }
        break;

      case 4:
      case 6:
      case 9:
      case 11:
        if (day >= 30) {
          dayNext = 1;
          monthNext += 1;
        } else if (day <= 1) {
          dayPre = 31;
          monthPre -= 1;
        }
        break;

      //Tháng 2
      case 2:
        if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
          if (dayNext > 29) {
            dayNext = 1;
            monthNext += 1;
          }
        } else {
          if (dayNext > 28) {
            dayNext = 1;
            monthNext += 1;
          }
        }
        if (day <= 1) {
          dayPre = 31;
          monthPre -= 1;
        }
        break;
    }

    console.log({ dayPre, monthPre, yearPre });
    console.log({ dayNext, monthNext, yearNext });

    document.getElementById(
      "result1_1"
    ).innerHTML = `Ngày hôm trước: ${dayPre}/${monthPre}/${yearPre}`;
    document.getElementById(
      "result1_2"
    ).innerHTML = `Ngày hôm sau: ${dayNext}/${monthNext}/${yearNext}`;
  }
}

function ex2() {
  var month = document.getElementById("txt-month-2")?.value * 1;
  var year = document.getElementById("txt-year-2")?.value * 1;
  var day;
  switch (month) {
    case 1:
    case 3:
    case 5:
    case 7:
    case 8:
    case 10:
    case 12:
      day = 31;
      break;
    case 4:
    case 6:
    case 9:
    case 11:
      day = 30;
      break;
    case 2:
      if (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0)) {
        day = 29;
      } else {
        day = 28;
      }
      break;
  }
  document.getElementById(
    "result2"
  ).innerHTML = `Tháng ${month}/${year} có ${day} ngày`;
}

function ex3() {
  var num = document.getElementById("txt-number")?.value * 1;
  var donVi = num % 10;
  var chuc = ((num % 100) - donVi) / 10;
  var tram = Math.floor(num / 100);
  console.log({ num, tram, chuc, donVi });

  var speakTram = convert(tram) + " trăm";
  var speakChuc =
    chuc === 0 ? " lẻ" : chuc === 1 ? " mười" : convert(chuc) + " mươi";
  var speakDonVi = donVi === 0 ? "" : convert(donVi);
  document.getElementById(
    "result3"
  ).innerHTML = `Cách đọc: ${speakTram} ${speakChuc} ${speakDonVi}`;
}

function convert(number) {
  switch (number) {
    case 1:
      return `một`;
    case 2:
      return `hai`;
    case 1:
      return `một`;
    case 3:
      return `ba`;
    case 4:
      return `bốn`;
    case 5:
      return `năm`;
    case 6:
      return `sáu`;
    case 7:
      return `bảy`;
    case 8:
      return `tám`;
    case 9:
      return `chín`;
  }
}

function ex4() {
  var nameMaxDis = null;
  //persom1
  var name1 = document.getElementById("txt-name1")?.value;
  var x1 = document.getElementById("txt-x1")?.value * 1;
  var y1 = document.getElementById("txt-y1")?.value * 1;

  //persom2
  var name2 = document.getElementById("txt-name2")?.value;
  var x2 = document.getElementById("txt-x2")?.value * 1;
  var y2 = document.getElementById("txt-y2")?.value * 1;

  //persom3
  var name3 = document.getElementById("txt-name3")?.value;
  var x3 = document.getElementById("txt-x3")?.value * 1;
  var y3 = document.getElementById("txt-y3")?.value * 1;

  //persom_-school
  var name_school = document.getElementById("txt-name-school")?.value;
  var x_school = document.getElementById("txt-x-school")?.value * 1;
  var y_school = document.getElementById("txt-y-school")?.value * 1;

  var d1 = d(x1, y1, x_school, y_school);
  var d2 = d(x2, y2, x_school, y_school);
  var d3 = d(x3, y3, x_school, y_school);

  if (d1 > d2 && d1 > d3) {
    nameMaxDis = name1;
  } else if (d2 > d1 && d1 > d3) {
    nameMaxDis = name2;
  } else {
    nameMaxDis = name3;
  }

  console.log(x2, y2);
  console.log({ d1, d2, d3 });

  document.getElementById(
    "result4"
  ).innerHTML = `Nhà ${nameMaxDis} xa trường ${name_school} nhất!`;
}

function d(x1, y1, x2, y2) {
  return Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2));
}
